import React from 'react';

import 'App.css';
import gearImage from 'gear.png';

const BoringGear = () => (
    <img className="gear boring-gear" src={gearImage} />
);

export default BoringGear;
