import React, { Component } from 'react';

import config from 'config';

class Gear extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: ""
        };

        this.cycleNamesInterval = null;

        this.chooseSacrifice = this.chooseSacrifice.bind(this);
        this.cycleNames = this.cycleNames.bind(this);
        this.unsetIntervals = this.unsetIntervals.bind(this);

        setTimeout(() => {
            this.unsetIntervals();
            this.chooseSacrifice();
        }, config.RUN_TIME);
    }

    chooseSacrifice() {
        const { choose } = this.props;
        const { name } = this.state;

        choose(name);
    }

    cycleNames(names) {
        this.cycleNamesInterval = setInterval(() => {
            this.setState({ name: names[Math.floor(Math.random() * names.length)] });
        }, config.CYCLE_NAMES_DEFAULT_TIME);
    }

    unsetIntervals() {
        clearInterval(this.cycleNamesInterval);
    }
}

export default Gear;
