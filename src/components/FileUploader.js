import React, { Component } from 'react';
import Dropzone from 'react-dropzone';

class FileUploader extends Component {
    constructor(props) {
        super(props);

        this.initReader = this.initReader.bind(this);
        this.parseFile = this.parseFile.bind(this);
        this.uploadFile = this.uploadFile.bind(this);
    }

    componentWillMount() {
        this.initReader();
    }

    initReader() {
        const { returnData } = this.props;

        this.reader = new FileReader();

        this.reader.onload = () => {
            const data = this.parseFile(this.reader.result);
            returnData(data);
        };

        this.reader.onabort = () => console.log('file reading was aborted');
        this.reader.onerror = () => console.log('file reading has failed');
    }

    // Assuming file is of the form "data1\ndata2n..."
    parseFile(file) {
        const names = file.split('\n');

        // Accounts for automatic newlines at EOF
        if (names[names.length - 1] == "")
            names.pop();

        return names;
    }

    uploadFile(files) {
        files.forEach(file => this.reader.readAsText(file));
    }

    render() {
        return (
            <Dropzone onDrop={this.uploadFile}>
                <p>Upload file</p>
            </Dropzone>
        );
    }
}

export default FileUploader;
