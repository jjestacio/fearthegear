import React from 'react';

import 'App.css';
import gearImage from 'gear.png';

import Gear from './Gear';

class NormalGear extends Gear {
    constructor(props) {
        super(props);

    }

    componentWillMount() {
        const { names } = this.props;
        this.cycleNames(names);
    }

    render() {
        const { name } = this.state;

        return (
            <div className="page normal-page">
                <div className="name">{ name }</div>
                <img className="gear normal-gear" src={gearImage} />
            </div>
        );
    }
}

export default NormalGear;
