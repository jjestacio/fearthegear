import React, { Component } from 'react';

import FileUploader from 'components/FileUploader';
import Gear from 'components/Gear';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            names: ["mom", "dad", "jayana", "jj"],
        };

        this.setNames = this.setNames.bind(this);
    }

    setNames(names) {
        this.setState({ names });
    }

    render() {
        const { names } = this.state;

        if (!names.length)
            return <FileUploader returnData={names => this.setNames(names)} />

        return (
            <div className="app">
                <Gear names={names} />
            </div>
        );
    }
}
