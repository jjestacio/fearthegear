import React, { Component } from 'react';

import BoringGear from './BoringGear';
import NormalGear from './NormalGear';

const modes = {
    BORING: "boring",
    NORMAL: "normal",
    FUN: "fun",
};

class GearContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mode: modes.BORING,
            names: [],
            name: "",
            running: false,
        };

        this.choose = this.choose.bind(this);
        this.getGear = this.getGear.bind(this);
    }

    componentWillMount() {
        document.addEventListener("keydown", e => {
            const key = e.key.toLowerCase();
            let mode = undefined;

            switch (key) {
                case "n": mode = modes.NORMAL; break;
                case "f": mode = modes.FUN; break;
            }

            mode && this.startGear(mode);
        });
    }

    choose(name) {
        this.setState({ mode: modes.BORING, name, running: false });
        // TODO: Choose winner animation
    }

    getGear() {
        const { mode, names } = this.state;

        switch (mode) {
            case modes.NORMAL:
                return <NormalGear names={names} choose={name => this.choose(name)} />
            case modes.BORING:
                return <BoringGear />
        }

        return "no gear found";
    }

    startGear(mode) {
        const { names } = this.props;
        this.setState({ mode, name: "", names, running: true });
    }

    render() {
        const { name, running } = this.state;

        if (running)
            return this.getGear();

        return (
            <div className="page main-page">
                <div className="chosen-sacrifice">{ name }</div>
                <BoringGear />
            </div>
        );
    }
}

export default GearContainer;
